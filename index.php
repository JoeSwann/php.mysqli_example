<?php

//Load config
require_once('conf.php');

//load our database class
require_once('lib/connect.php');

//startup sequence, connect to databases
//set up sessions etc etc
$connect = new Db();
$db = &$connect->get_db();

if(!$db) die('no database connection');

//routes would go here, most php apps take get 
//variables (?route=users) and use them to redirect to pages

//Essentially this is the user controller 
//could move this to a file /controllers/user.php
require_once('models/users.php');
$user_model = new Users(&$db);
$users = $user_model->get_users();

//And this would go in /views/user.php
echo "Users \n";
var_dump($users);

//Then we run our shutdown sequence
$connect->close();