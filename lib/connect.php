<?php

Class Db {
  var $db;

  function __construct() {
    $config = Config::get_config();
    $sql_conf = $config['mysql'];

    //set up our database connection
    $db = new mysqli($sql_conf['host'], $sql_conf['user'], $sql_conf['pass'], $sql_conf['name']);

    if($db->connect_errno > 0) {
      die('Unable to connect to database [' . $db->connect_error . ']');
    }

    //so we can access this variable
    $this->db = &$db;
  }

  public function get_db() {
    return $this->db;
  }

  public function close() {
    $this->db->close();
  }
}