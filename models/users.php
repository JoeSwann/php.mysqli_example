<?php

Class Users {
  var $db;

  function __construct($db = false) {
    $this->db = &$db;
  }

  /*
    This function could be moved into a parent class,
    so you would have Class Users extends Model
    and then basic stuff like query() would
    be the same everwhere, DRY!
  */

  private function query($query = '') {
    //you can validate + escape the query here
    if(!$query) return false;

    //do the query
    $result = $this->db->query($query);

    if(!$result) {
      //log helpful error messages
      return false;
    }

    //else I guess we made it
    return $result;
  }

  public function get_users() {
    $users = array();

    //do query
    $query = "SELECT * FROM users";
    $result = $this->query($query);
    
    if(!$result) return false;

    while($user = $result->fetch_array()) {
      $users[] = $user;
    }

    return $users;
  }
}