<?php
//create a version of this file called conf.php with the right settings

//MYSQL
Class Config {

  static function get_config() {
    //this could be dynamically loaded from anywhere
    //eg a .json file outside of the root folder
    return array(
      'mysql' => array(
        'host' => 'localhost',
        'user' => 'asdf',
        'pass' => 'asdf',
        'name' => 'asdf'
      )
    );
  }
}